//
//  OrderCafeViewModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/13/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation


class  OrderCafeViewModel {
    var id:String?
    var price:Float?
    var qty:Int?
    var cafeImage:String?
    
    init()  {}
    
    init (orderCafe:OrderCafeModel) {
        
        self.id = orderCafe.id
        self.price = orderCafe.price
        self.qty = orderCafe.qty
        self.cafeImage = orderCafe.cafeImage
    }
}
