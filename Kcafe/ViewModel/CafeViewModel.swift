//
//  CafeViewModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/5/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation


class  CafeViewModel {
    var id:String?
    var name:String?
    var rating:Float?
    var price:Float?
    var image:String?

    
    init(cafe:Cafe) {
        self.id = cafe.id
        self.name = cafe.name
        self.rating = cafe.rating
        self.price = cafe.price
        self.image = cafe.image
    }
}
