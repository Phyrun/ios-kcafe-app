//
//  SuccessOrderListViewModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/21/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation


class SuccessOrderListViewModel {
    var id:String?
    
    func successOrders(completion:@escaping (_ successOrderViewModel:SuccessOrderViewModel)->Void) {
        
        SignInService.shared.successeOrder { (successOrderModel) in
            let successOrderViewModel = SuccessOrderViewModel(SuccessOrder: successOrderModel)
            
            completion(successOrderViewModel)
        }
    }
}
