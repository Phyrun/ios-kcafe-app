//
//  CafeListViewModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/5/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation


class  CafeListViewModel {
    func cafes(completion:@escaping (_ cafeViewMovdel:[CafeViewModel])->Void) {
        
        CafeService.shared.cafes { (coffees) in
            let coffeeViewModel:[CafeViewModel] = coffees.compactMap(CafeViewModel.init)
            
            completion(coffeeViewModel)
        }
        
    }
//    cafe category
    
    func cafeCategory(categoryTitle:String, completion:@escaping (_ cafeViewMovdel:[CafeViewModel])->Void) {
        CafeService.shared.cafeCategory(categoryTitle: categoryTitle) { (coffees) in
            let coffeeViewModel:[CafeViewModel] = coffees.compactMap(CafeViewModel.init)
     
        completion(coffeeViewModel)
        }
        
    }
}
