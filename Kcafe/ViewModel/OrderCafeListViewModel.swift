//
//  OrderCafeListViewModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/13/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation

class OrderCafeListViewModel {
    func orderCafes(completion:@escaping (_ OrderCafeViewMovdel:[OrderCafeViewModel])->Void) {
        
        OrderCafeService.shared.OrderCafe { (orderCafeModel) in
            let cafeViewModel:[OrderCafeViewModel] = orderCafeModel.compactMap(OrderCafeViewModel.init)
            
            completion(cafeViewModel)

        }
    }
}

    

