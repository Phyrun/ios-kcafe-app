//
//  SuccessOrderViewModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/21/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//


import Foundation
import SwiftyJSON

class SuccessOrderViewModel {
    
    var objectId:String?
    var firstName:String?
    var lastName:String?
    var email:String?
    var image:String?
    var phone:String?
    
    
    init(SuccessOrder:SignInModel) {
        self.objectId = SuccessOrder.objectId
        self.firstName = SuccessOrder.firstName
        self.lastName = SuccessOrder.lastName
        self.email = SuccessOrder.email
        self.image = SuccessOrder.image
        self.phone = SuccessOrder.phone
        
        
    }
    
}
