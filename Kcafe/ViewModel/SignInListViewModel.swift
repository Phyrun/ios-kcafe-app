//
//  SignInListViewModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/1/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation

class SignInListViewModel {
    func fetchSignIns(username:String, password:String, completion:@escaping (_ signInViewModels:SignInViewModel)->Void) {
        
        SignInService.shared.fetchSignIn(username: username, password: password){ (singIn) in
            
            let signInViewModel = SignInViewModel(signIn: singIn)
            
            completion(signInViewModel)
        }
    }
}
