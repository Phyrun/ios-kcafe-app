//
//  CategoryTableViewCell.swift
//  Kcafe
//
//  Created by Phyrun on 8/10/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

  
   
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
