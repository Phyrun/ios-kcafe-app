//
//  CategoryDetailTableViewCell.swift
//  Kcafe
//
//  Created by Phyrun on 8/10/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit
import Kingfisher


class CategoryDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var cafePriceLabel: UILabel!
    @IBOutlet weak var cafeRatingLabel: UILabel!
    @IBOutlet weak var cafeNameLabel: UILabel!
    @IBOutlet weak var cafeImageView: UIImageView!
    
    @IBOutlet weak var categoryOrderButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(cafeViewModel: CafeViewModel) {
        cafeNameLabel.text = cafeViewModel.name
        cafeRatingLabel.text = String(format: "%.2f", cafeViewModel.rating!)
        cafePriceLabel.text = String(format: "%.2f", cafeViewModel.price!)
        
        let url = URL(string: cafeViewModel.image!)
        cafeImageView?.kf.setImage(with: url)
        
    }

}
