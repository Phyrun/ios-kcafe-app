//
//  CafeTableViewCell.swift
//  Kcafe
//
//  Created by Phyrun on 8/6/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit
import Kingfisher

class CafeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratinglabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var cafeImageVeiw: UIImageView!
    
    @IBOutlet weak var orderButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func bindData(cafeViewModel: CafeViewModel) {
        nameLabel.text = cafeViewModel.name
        ratinglabel.text = String(format: "%.2f", cafeViewModel.rating!)
        priceLabel.text = String(format: "%.2f", cafeViewModel.price!)
        
        let url = URL(string: cafeViewModel.image!)
        cafeImageVeiw?.kf.setImage(with: url)
        
    }
}
