//
//  OrderCafeTableViewCell.swift
//  Kcafe
//
//  Created by Phyrun on 8/13/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit
import Kingfisher

class OrderCafeTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteOrderImageView: UIImageView!
    @IBOutlet weak var orderImageView: UIImageView!
    @IBOutlet weak var orderPriceLabel: UILabel!
    @IBOutlet weak var orderQtyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindData(orderCafeViewModel: OrderCafeViewModel) {
        
        orderPriceLabel.text = String(format: "%.2f", orderCafeViewModel.price!)
        orderQtyLabel.text = String(orderCafeViewModel.qty!)
        
        let url = URL(string: orderCafeViewModel.cafeImage!)
        orderImageView?.kf.setImage(with: url)
        
    }

}
