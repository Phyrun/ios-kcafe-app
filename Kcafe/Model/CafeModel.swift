//
//  CafeModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/5/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import SwiftyJSON


class Cafe {
    var id:String?
    var name:String?
    var rating:Float?
    var price:Float?
    var image:String?
    var phone:Float?
    
    init()  {}
    
    init (json:JSON) {
        self.id = json["objectId"].string
        self.name = json["name"].string
        self.rating = json["rating"].float
        self.price = json["price"].float
        self.image = json["image"]["url"].string
        self.phone = json["phone"].float
    }
}
