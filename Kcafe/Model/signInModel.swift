//
//  signInModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/1/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import SwiftyJSON

class SignInModel {
    var objectId:String?
    var firstName:String?
    var lastName:String?
    var email:String?
    var image:String?
    var phone:String?
    
    init() {}
    
    init(json:JSON) {
        self.objectId = json["objectId"].string
        self.firstName = json["firstName"].string
        self.lastName = json["lastName"].string
        self.email = json["email"].string
        self.image = json["image"].string
        self.phone = json["phone"].string
        
    }
}
