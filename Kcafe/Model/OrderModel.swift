//
//  OrderModel.swift
//  Kcafe
//
//  Created by Phyrun on 8/13/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import SwiftyJSON


class OrderCafeModel {
    var id:String?
    var price:Float?
    var qty:Int?
    var cafeImage:String?
    
    init()  {}
    
    init (json:JSON) {
        self.id = json["objectId"].string
        self.price = json["price"].float
        self.qty = json["qty"].int
        self.cafeImage = json["cafe"]["image"]["url"].string
    }
}
