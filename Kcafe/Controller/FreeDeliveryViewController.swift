//
//  FreeDeliveryViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/22/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class FreeDeliveryViewController: UIViewController {

    @IBOutlet weak var deliverySuccess: UIButton!
    let delivery =  UIColor(red: 81/255,  green: 174/255, blue: 245/255, alpha: 1)

    override func viewDidLoad() {
        super.viewDidLoad()

        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "backcafe-2")
        backgroundImage.contentMode =  UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        // Do any additional setup after loading the view.
        deliverySuccess.backgroundColor = .clear
        deliverySuccess.layer.cornerRadius = deliverySuccess.frame.height/2
        deliverySuccess.layer.borderWidth = 1
        deliverySuccess.layer.backgroundColor = delivery.cgColor
        deliverySuccess.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func ContinueToHomeAction(_ sender: Any) {

        tabBarController?.selectedIndex = 0
    }
    
}
