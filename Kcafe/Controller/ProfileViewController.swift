//
//  ProfileViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/21/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var logOutBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var gmailImageView: UIImageView!
    @IBOutlet weak var emailImageView: UIImageView!
    @IBOutlet weak var phyrunImageView: UIImageView!
    @IBOutlet weak var nameImageView: UIImageView!
    var successOrderListViewModel:SuccessOrderListViewModel?
 
    override func viewDidLoad() {
        super.viewDidLoad()

        print("hii")
        successOrderListViewModel = SuccessOrderListViewModel()
        successOrderListViewModel?.successOrders(completion: { (successOrderViewModel) in
            
            let url = URL(string: successOrderViewModel.image!)
            self.imageView?.kf.setImage(with: url)
            self.firstNameLabel.text = successOrderViewModel.firstName
            self.lastNameLabel.text = successOrderViewModel.lastName
            self.emailLabel.text = successOrderViewModel.email
        })
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchUser), name: NSNotification.Name("userUpdate"), object: nil)
        
        image(imageName: "man-user", imageView: nameImageView)
        image(imageName: "name", imageView: phyrunImageView)
        image(imageName: "close-envelope", imageView: gmailImageView)
    }
    @IBAction func EditProfileAction(_ sender: Any) {
        performSegue(withIdentifier: "editProfile", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editProfile" {
            let profile = segue.destination as! EditProfileViewController
            
            successOrderListViewModel = SuccessOrderListViewModel()

            successOrderListViewModel?.successOrders(completion: { (successOrderViewModel) in
                profile.firstNameTextField.text = successOrderViewModel.firstName
                profile.lastNameTextField.text = successOrderViewModel.lastName
                profile.emailTextField.text = successOrderViewModel.email
            })
           
        }
    }
    @IBAction func LogOutAction(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isLogin")
        
        self.present(UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "backHome"), animated: true, completion: nil) 
     
    }
    @objc func fetchUser() {
        let successOrderListViewModel = SuccessOrderListViewModel()
        
        successOrderListViewModel.successOrders(completion: { (successOrderViewModel) in
            self.firstNameLabel.text = successOrderViewModel.firstName
            self.lastNameLabel.text = successOrderViewModel.lastName
            self.emailLabel.text = successOrderViewModel.email
        })
    }
    @objc func image (imageName:String, imageView:UIImageView){
        
        let categoryImage = UIImage (named: imageName)
        
        imageView.image =  categoryImage
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
    }
}
