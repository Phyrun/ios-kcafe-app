//
//  SignUpViewController.swift
//  Kcafe
//
//  Created by Phyrun on 7/25/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    var signUpService:SignUpService?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let signUp =  UIColor(red: 81/255,  green: 174/255, blue: 245/255, alpha: 1)
        
        signUpButton.backgroundColor = .clear
        signUpButton.layer.cornerRadius = 25
        signUpButton.layer.borderWidth = 1
        signUpButton.layer.backgroundColor = signUp.cgColor
        signUpButton.layer.borderColor = UIColor.white.cgColor
    }

    @IBAction func signUpAction(_ sender: Any) {
        
        
        print(firstNameTextField.text)
        print(lastNameTextField.text)
        print(passwordTextField.text)
        print(emailTextField.text)
        
        if firstNameTextField.text != "" && lastNameTextField.text != "" && passwordTextField.text != "" && emailTextField.text != "" {
            
            signUpService = SignUpService()
            signUpService?.signUp(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!,password: passwordTextField.text!, email: emailTextField.text!, phone: phoneNumberTextField.text!)
            performSegue(withIdentifier: "goSignIn", sender: nil)
        } else {
            
            self.errorMessageLabel.isHidden = false
            
        }
    }
    
}
