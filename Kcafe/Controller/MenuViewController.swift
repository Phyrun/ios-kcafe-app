//
//  MenuViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/10/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class MenuViewController:UIViewController, UITableViewDelegate, UITableViewDataSource {
    var categories:[CategoryModel] = [
        CategoryModel.init(name: "Smothie", image: "smothie"),
        CategoryModel.init(name: "Hot", image: "hot"),
        CategoryModel.init(name: "iCE", image: "ice"),
        CategoryModel.init(name: "Juice", image: "juice"),
        CategoryModel.init(name: "Frappe", image: "frappe"),
        CategoryModel.init(name: "Tea", image: "tea")
      
    ]
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        
        // Do any additional setup after loading the view.
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoryTableViewCell
        cell.categoryNameLabel.text = categories[indexPath.row].name
        let categoryImage = UIImage(named: categories[indexPath.row].image)
        cell.categoryImageView.image =  categoryImage
        cell.categoryImageView.image = cell.categoryImageView.image?.withRenderingMode(.alwaysTemplate)
        cell.categoryImageView.tintColor = UIColor.red
       
    return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = tableView.indexPathForSelectedRow
        let categoryName = categories[(indexPath?.row)!].name
        if segue.identifier == "categoryDetail" {
            let dest = segue.destination as? CategoryDetailTableViewController
            dest?.categoryName = categoryName
        }
    }
}
