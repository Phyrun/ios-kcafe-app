//
//  HomeTableViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/2/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeTableViewController: UITableViewController {

    
    var cafeListViewModel:CafeListViewModel?
    var cafeViewModels:[CafeViewModel]?
    let orderHome =  UIColor(red: 81/255,  green: 174/255, blue: 245/255, alpha: 1)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        SVProgressHUD.setBackgroundColor(.black)
        SVProgressHUD.setForegroundColor(.white)
        SVProgressHUD.show()
        
        self.cafeListViewModel = CafeListViewModel()
        cafeListViewModel?.cafes(completion: { (coffeeViewModels) in
            self.cafeViewModels = coffeeViewModels
            print("hello")
            print(self.cafeViewModels?.count)
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
            }
        })
        getBadgeValue()
        
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let cafeViewModels = self.cafeViewModels {
            return cafeViewModels.count
        }
        return 0
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cafeCell", for: indexPath) as!
        CafeTableViewCell

            
            let cafeViewModel = self.cafeViewModels![indexPath.row]
            cell.bindData(cafeViewModel: cafeViewModel)
            cell.selectionStyle = .none
        
        let tappyCafe = CafeTap(target: self, action: #selector(placeOrder(_:)))
        cell.orderButton.addGestureRecognizer(tappyCafe)
        tappyCafe.cafeId = self.cafeViewModels![indexPath.row].id!
        tappyCafe.price = self.cafeViewModels![indexPath.row].price!
        
        cell.orderButton.backgroundColor = .clear
        cell.orderButton.layer.cornerRadius = cell.orderButton.frame.height/2
        cell.orderButton.layer.borderWidth = 1
        cell.orderButton.layer.backgroundColor = orderHome.cgColor
        cell.orderButton.layer.borderColor = UIColor.white.cgColor
        
        return cell
    }
  
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
   @objc func placeOrder(_ sender:CafeTap) {
    
        let cafeServivce = CafeService()
        cafeServivce.orderCafe(price: sender.price, qty: 1, cafe: sender.cafeId)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            NotificationCenter.default.post(name: NSNotification.Name("orderAdded"), object: nil)
            self.getBadgeValue()
        }
    }
    
    @objc func getBadgeValue() {
       
        let orderCafeListViewModel = OrderCafeListViewModel()
        orderCafeListViewModel.orderCafes { (orderCafeViewModel) in
            let badgeValue = orderCafeViewModel.count
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[1]
                tabItem.badgeValue = String(badgeValue)
            }
        }
    }
}
class   CafeTap:UITapGestureRecognizer {
    var cafeId = String()
    var price = Float()

}

//class Cafe
