//
//  EditProfileViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/23/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {


    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func SaveProfileAction(_ sender: Any) {
        
        let updateUser = UpdateUserService()
        updateUser.updateUser(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, email: emailTextField.text!)
        let alertMessage = UIAlertController(title: "Update User", message: "Your User Update Has Been Success", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
               NotificationCenter.default.post(name: NSNotification.Name("userUpdate"), object: nil)
            })
        }
        alertMessage.addAction(okAction)
        self.present(alertMessage, animated: true, completion: nil)
    }
    
    
}
