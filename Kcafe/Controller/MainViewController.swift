//
//  MainViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/21/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var kcafeLabel: UILabel!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//
        let bgColor = UIColor(red: 81/255,  green: 174/255, blue: 245/255, alpha: 1)
        let emailColor = UIColor(red: 163/255, green: 137/255, blue: 65/255, alpha: 1)
        
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "backcafe-2")
        backgroundImage.contentMode =  UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        
        
        fbButton.backgroundColor = .clear
        fbButton.layer.cornerRadius = 25
        fbButton.layer.borderWidth = 1
        fbButton.layer.backgroundColor = bgColor.cgColor
        fbButton.layer.borderColor = UIColor.white.cgColor
     
        emailButton.backgroundColor = .clear
        emailButton.layer.cornerRadius = 25
        emailButton.layer.borderWidth = 1
        emailButton.layer.backgroundColor = emailColor.cgColor
        emailButton.layer.borderColor = UIColor.white.cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isLogin = UserDefaults.standard.bool(forKey: "isLogin")
        print(isLogin)
        if isLogin == true {
            print("userIslogin")
            performSegue(withIdentifier: "goToHome", sender: nil)
        }
    }

 
}

