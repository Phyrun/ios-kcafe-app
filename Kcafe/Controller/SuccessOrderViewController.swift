//
//  SuccessOrderViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/21/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit

class SuccessOrderViewController: UIViewController {

    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var successOrderButton: UIButton!
    var successOrderListViewModel:SuccessOrderListViewModel?

    var cafeArr: [String] = []
    var test:String?
    let orderSuccess =  UIColor(red: 81/255,  green: 174/255, blue: 245/255, alpha: 1)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("hii")
        successOrderListViewModel = SuccessOrderListViewModel()
        successOrderListViewModel?.successOrders(completion: { (successOrderViewModel) in
            self.firstNameTextfield.text = successOrderViewModel.firstName
            self.lastNameTextfield.text = successOrderViewModel.lastName
            self.emailTextfield.text = successOrderViewModel.email
            self.phoneTextField.text = successOrderViewModel.phone
            
        })
        
        successOrderButton.backgroundColor = .clear
        successOrderButton.layer.cornerRadius = successOrderButton.frame.height/2
        successOrderButton.layer.borderWidth = 1
        successOrderButton.layer.backgroundColor = orderSuccess.cgColor
        successOrderButton.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func DeliveryAction(_ sender: Any) {
        
        let saveOrderSuccess = SaveOrderSuccessService()
        let userId = UserDefaults.standard.string(forKey: "userId")
        let cafeId = cafeArr
        saveOrderSuccess.saveOrderSuccess(userId: userId!, cafeId: cafeId)
        
        print(cafeArr)
        
        let clearOrderSuccess = ClearOrderSuccessService()
        let saveOrderId = cafeArr
        clearOrderSuccess.clearOrderSuccess(saveOrderId: saveOrderId)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            NotificationCenter.default.post(name: NSNotification.Name("orderAdded"), object: nil)
            
            self.getBadgeValue()
        }
    }
    @objc func getBadgeValue() {
        
        let orderCafeListViewModel = OrderCafeListViewModel()
        orderCafeListViewModel.orderCafes { (orderCafeViewModel) in
            let badgeValue = orderCafeViewModel.count
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[1]
                tabItem.badgeValue = String(badgeValue)
            }
        }
    }
    
}
