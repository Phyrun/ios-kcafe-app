//
//  OrderCafeViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/13/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit
import SVProgressHUD

class OrderCafeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var orderCafeListViewModel:OrderCafeListViewModel?
    var orderCafeViewModel:[OrderCafeViewModel]?
    @IBOutlet weak var totalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         let checkOut =  UIColor(red: 81/255,  green: 174/255, blue: 245/255, alpha: 1)
        self.tableView.delegate = self
        self.tableView.dataSource = self

        tableView.tableFooterView = UIView(frame: .zero)
        SVProgressHUD.setBackgroundColor(.black)
        SVProgressHUD.setForegroundColor(.white)
        SVProgressHUD.show()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchOrder), name: NSNotification.Name("orderAdded"), object: nil)
        fetchOrder()
        
        checkOutButton.backgroundColor = .clear
        checkOutButton.layer.cornerRadius = checkOutButton.frame.height/2
        checkOutButton.layer.borderWidth = 1
        checkOutButton.layer.backgroundColor = checkOut.cgColor
        checkOutButton.layer.borderColor = UIColor.white.cgColor
        

    }
    @objc func fetchOrder() {
        self.orderCafeListViewModel = OrderCafeListViewModel()
        
        orderCafeListViewModel?.orderCafes(completion: { (orderCafeViewModel) in
            self.orderCafeViewModel = orderCafeViewModel
            self.totalLabel.text = String(orderCafeViewModel.count)
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
            }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let orderCafeViewModel = self.orderCafeViewModel {
        return orderCafeViewModel.count
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCafeCell", for: indexPath) as!
        OrderCafeTableViewCell
        
        let orderCafeViewModel = self.orderCafeViewModel![indexPath.row]
        cell.bindData(orderCafeViewModel: orderCafeViewModel)
        cell.selectionStyle = .none
        
        let deleteTap = DeleteOrderTap(target: self, action: #selector(deleteOrder(_:)))
        cell.deleteOrderImageView.addGestureRecognizer(deleteTap)
    
        deleteTap.saveOrderId = self.orderCafeViewModel![indexPath.row].id!

        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    @IBAction func CheckOutAction(_ sender: Any) {
        performSegue(withIdentifier: "checkOut", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "checkOut" {
             let cafe = segue.destination as! SuccessOrderViewController
            for cf in self.orderCafeViewModel! {
                cafe.cafeArr.append(cf.id!)
            }
        }
    }
    @objc func deleteOrder(_ sender:DeleteOrderTap) {
        print(sender.saveOrderId)
        
        let deleteOrder = DeleteOrderService()
        deleteOrder.deleteOrder(saveOrderId: sender.saveOrderId)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            self.fetchOrder()
            self.getBadgeValue()
        }
    }
    
   func getBadgeValue() {
        
        let orderCafeListViewModel = OrderCafeListViewModel()
        orderCafeListViewModel.orderCafes { (orderCafeViewModel) in
            let badgeValue = orderCafeViewModel.count
            if let tabItems = self.tabBarController?.tabBar.items {
                let tabItem = tabItems[1]
                tabItem.badgeValue = String(badgeValue)
            }
        }
    }
}
class   DeleteOrderTap:UITapGestureRecognizer {
    var saveOrderId = String()
    
    
}
