//
//  SignInViewController.swift
//  Kcafe
//
//  Created by Phyrun on 8/1/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignInViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var errorMessageLabel: UILabel!
    var signInListViewModels:SignInListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        let signIn =  UIColor(red: 81/255,  green: 174/255, blue: 245/255, alpha: 1)
        
        signInButton.backgroundColor = .clear
        signInButton.layer.cornerRadius = 25
        signInButton.layer.borderWidth = 1
        signInButton.layer.backgroundColor = signIn.cgColor
        signInButton.layer.borderColor = UIColor.white.cgColor
        
    }

    @IBAction func SignInAction(_ sender: Any) {
        
        self.signInListViewModels = SignInListViewModel()
        signInListViewModels?.fetchSignIns(username: emailTextField.text!, password: passwordTextField.text!, completion: { (signInViewModel) in
            
            if let userId = signInViewModel.objectId {
                UserDefaults.standard.set(true, forKey: "isLogin")
                UserDefaults.standard.set(userId, forKey: "userId")

                self.performSegue(withIdentifier: "goHome", sender: nil)
                
            } else {
                print("invalid email/password")
                self.errorMessageLabel.text = "indvalid email/password"
                self.errorMessageLabel.isHidden = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.errorMessageLabel.isHidden = true
                })
            }
        })
       
    }
    
}
