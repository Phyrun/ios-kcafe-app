//
//  CafeService.swift
//  Kcafe
//
//  Created by Phyrun on 8/5/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CafeService {
    
    // Defined shared instance
    static let shared = CafeService()
    
    let BASE_URL = "https://ione-app-api-testing.ioneservice.com/parse/functions/"
    let headers:[String:String] = [
        
        "X-Parse-Application-Id": "myAppID",
        "Content-Type":"application/json",
        "Accept":"application/json"
        
    ]
    func cafes (completion:@escaping (_ coffee:[Cafe])->Void){
        let GET_SIGNUP_URL = BASE_URL + "list_cafe"
        
       
        Alamofire.request( GET_SIGNUP_URL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                
                if let data = respone.data {
                    let json = try? JSON(data: data)
                    
                    var cafes = [Cafe]()
                    
//                  "  result " get from api key
                    for cafesJSON in json!["result"].array! {
                        cafes.append(Cafe(json: cafesJSON))
                    }
                    print(cafes.first?.name)
                    completion(cafes)
                }
            }else {
                print("Get Cafe List Fail")
            }
        }
    }
//    cafecategory
    func cafeCategory (categoryTitle:String, completion:@escaping (_ coffee:[Cafe])->Void){
        let GET_SIGNUP_URL = BASE_URL + "cafe_category"
        
        let parameters:[String:Any?] = [
            
            "categoryTitle": categoryTitle
            
        ]
        
        Alamofire.request( GET_SIGNUP_URL, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                
                if let data = respone.data {
                    let json = try? JSON(data: data)
                    
                    var cafes = [Cafe]()
                    
                    //                  "  result " get from api key
                    for cafesJSON in json!["result"].array! {
                        cafes.append(Cafe(json: cafesJSON))
                    }
                    print(cafes.first?.name)
                    completion(cafes)
                }
            }else {
                print("Get Cafe Category Fail")
            }
        }
    }
//    order cafe
    
    func orderCafe (price:Float, qty:Int, cafe:String){
        let GET_SIGNUP_URL = BASE_URL + "order_cafe"
        
        let parameters:[String:Any?] = [
            
            "price": price,
            "qty": qty,
            "cafeId": cafe,
            "userId": UserDefaults.standard.string(forKey: "userId")!
            
        ]
        
        Alamofire.request( GET_SIGNUP_URL, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                print(respone)
            }else {
                print("Get Order Cafe Fail")
            }
        }
    }
}
