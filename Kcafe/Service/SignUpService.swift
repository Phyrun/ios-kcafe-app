//
//  SignUpService.swift
//  Kcafe
//
//  Created by Phyrun on 8/1/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SignUpService {
    let BASE_URL = "https://ione-app-api-testing.ioneservice.com/parse/functions/"
    let headers:[String:String] = [
        
        "X-Parse-Application-Id": "myAppID",
        "Content-Type":"application/json",
        "Accept":"application/json"
        
    ]
    func signUp(firstName:String, lastName:String, password:String, email:String, phone:String){
        let GET_SIGNUP_URL = BASE_URL + "sign_up"
        
        let parameters:[String:Any?] = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "password": password,
            "phone": phone
        ]
        Alamofire.request( GET_SIGNUP_URL, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                print("New sign Up Success")
            }else {
                print("New sign In Fail")
            }
        }
    }
}
