//
//  SignInService.swift
//  Kcafe
//
//  Created by Phyrun on 8/1/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SignInService {
    
    // Defined shared instance
    static let shared = SignInService()
    
    let BASE_URL = "https://ione-app-api-testing.ioneservice.com/parse/functions/"
    let headers:[String:String] = [
        
        "X-Parse-Application-Id": "myAppID",
        "Content-Type":"application/json",
        "Accept":"application/json"
        
    ]
    func fetchSignIn( username:String, password:String,completion:@escaping (_ signIns:SignInModel)->Void){
        let GET_SIGNUP_URL = BASE_URL + "sign_in"
        
        let parameters:[String:Any?] = [
            
            "username": username,
            "password": password
        ]
        Alamofire.request( GET_SIGNUP_URL, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            print(respone)
            if respone.result.isSuccess {
                if let data = respone.data {
                    
                    let json = try? JSON(data: data)
                    
                    let signIn = SignInModel(json: json!["result"])
                    
                    completion(signIn)
                    
                }
                
            }else {
                print("New sign In Fail")
            }
        }
    }
    func successeOrder (completion:@escaping (_ signIns:SignInModel)->Void){
        let GET_SUCCESS_URL = BASE_URL + "get_single_user"
        
        let parameters:[String:String] = [
            
            "userId": UserDefaults.standard.string(forKey: "userId")!
            
        ]
        
        Alamofire.request( GET_SUCCESS_URL, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                if let data = respone.data {
//                    print(respone)
                    let json = try? JSON(data: data)
                    
                    let successOrder = SignInModel(json: json!["result"][0])
                    
                    //                  "  result " get from api key
                   
                    completion(successOrder)
                    
                }
                
            }else {
                print("New Success Order Fail")
            }
        }
    }
}

