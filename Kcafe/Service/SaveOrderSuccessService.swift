//
//  SaveOrderSuccessService.swift
//  Kcafe
//
//  Created by Phyrun on 8/22/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SaveOrderSuccessService {
    let BASE_URL = "https://ione-app-api-testing.ioneservice.com/parse/functions/"
    let headers:[String:String] = [
        
        "X-Parse-Application-Id": "myAppID",
        "Content-Type":"application/json",
        "Accept":"application/json"
        
    ]
    func saveOrderSuccess(userId:String, cafeId:Array<Any>){
        let GET_SAVE_ORDER_SUCCESS = BASE_URL + "save_order_success"
        
        let parameters:[String:Any?] = [
            "userId": userId,
            "cafeId": cafeId,
        ]
        Alamofire.request( GET_SAVE_ORDER_SUCCESS, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                print("Save Order Success")
            }else {
                print("Save Order Fail")
            }
        }
    }
}
