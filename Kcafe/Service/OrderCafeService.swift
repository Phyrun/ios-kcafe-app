//
//  OrderCafeService.swift
//  Kcafe
//
//  Created by Phyrun on 8/13/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class OrderCafeService {
    
    // Defined shared instance
    static let shared = OrderCafeService()
    
    let BASE_URL = "https://ione-app-api-testing.ioneservice.com/parse/functions/"
    let headers:[String:String] = [
        
        "X-Parse-Application-Id": "myAppID",
        "Content-Type":"application/json",
        "Accept":"application/json"
        
    ]
    
    func OrderCafe (completion:@escaping (_ coffee:[OrderCafeModel])->Void){
        let GET_ORDER_CAFE_URL = BASE_URL + "get_order_cafe"
        let parameters:[String:Any?] = [
           
            "userId": UserDefaults.standard.string(forKey: "userId")!
        ]
        
        Alamofire.request(GET_ORDER_CAFE_URL, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                
                if let data = respone.data {
                    let json = try? JSON(data: data)
                    
                    var orderCafes = [OrderCafeModel]()
                    
                    //                  "  result " get from api key
                    for cafesJSON in json!["result"].array! {
                        orderCafes.append(OrderCafeModel(json: cafesJSON))
                    }
           
                    completion(orderCafes)
                }
            }else {
                print("Get Order Cafe Fail")
            }
        }
    }
}
