//
//  UpdateUserService.swift
//  Kcafe
//
//  Created by Phyrun on 8/24/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UpdateUserService {
    func updateUser(firstName:String, lastName:String, email:String){
        let signUp = SignUpService()
       
        let UPDATE_USER =  signUp.BASE_URL + "update_user"
        
        let parameters:[String:Any?] = [
            
            "userId": UserDefaults.standard.string(forKey: "userId"),
            "firstName": firstName,
            "lastName": lastName,
            "email": email
        ]
        Alamofire.request( UPDATE_USER, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: signUp.headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                print("Update User Success")
            }else {
                print("Update User Fail")
            }
        }
    }
}
