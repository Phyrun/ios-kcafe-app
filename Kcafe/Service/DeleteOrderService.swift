//
//  DeleteOrderService.swift
//  Kcafe
//
//  Created by Phyrun on 8/23/19.
//  Copyright © 2019 Phyrun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DeleteOrderService {
    let BASE_URL = "https://ione-app-api-testing.ioneservice.com/parse/functions/"
    let headers:[String:String] = [
        
        "X-Parse-Application-Id": "myAppID",
        "Content-Type":"application/json",
        "Accept":"application/json"
        
    ]
    func deleteOrder(saveOrderId:String){
        let DELETE_ORDER_SERVICE = BASE_URL + "delete_order"
        
        let parameters:[String:Any?] = [
            "saveOrderId": saveOrderId,
        ]
        Alamofire.request( DELETE_ORDER_SERVICE, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ (respone)in
            if respone.result.isSuccess {
                print("Delete Order Success")
            }else {
                print("Delete Order Fail")
            }
        }
    }
}
